<?php

namespace Tests\Feature;

use App\Contact;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $user = new User([
            'email' => 'test@example.com',
            'name' => 'testuser',
            'password' => '123456',
        ]);
        $user->save();
        $contact = new Contact([
            'name' => 'Test',
            'email' => 'test1@example.com',
            'phone' => '052 511 87 80',
            'company_id' => 1
        ]);
        $contact->save();
        $this->token = auth()->login($user);
    }

    /** @test */
    public function it_will_create_a_contact()
    {
        $response = $this->post('api/contact', [
            'name' => 'Test Contact',
            'email' => 'test@example.com',
            'phone' => '012 345 67 89'
        ]);
        $response->assertCreated();
    }

    /** @test */
    public function it_will_list_contacts()
    {
        $response = $this->get('api/contact');
        $response->assertJson([[
            'phone' => '052 511 87 80'
        ]]);
    }

    /** @test */
    public function it_will_display_one_contact()
    {
        $response = $this->get('api/contact/1');
        $response->assertJsonFragment([
            'phone' => '052 511 87 80'
        ]);
    }

    /** @test */
    public function it_will_delete_a_contact()
    {
        $response = $this->delete('api/contact/1');
        $response->assertOk();
        $response = $this->get('api/contact');
        $response->assertJson([]);
    }

    /** @test */
    public function it_will_edit_a_contact()
    {
        $response = $this->put('api/contact/1', [ 'name' => 'New Testname' ]);
        $response->assertJson([
            'name' => 'New Testname'
        ]);
    }

    /** @test */
    public function it_will_not_add_unknown_attributes()
    {
        $response = $this->put('api/contact/1', [ 'notExistingAttr' => 'Nonsense' ]);
        $response->assertStatus(400);
    }

}
