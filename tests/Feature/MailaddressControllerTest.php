<?php

namespace Tests\Feature;

use App\Mailaddress;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MailaddressControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $user = new User([
            'email' => 'test@example.com',
            'name' => 'testuser',
            'password' => '123456',
        ]);
        $user->save();
        $this->token = auth()->login($user);

        Mailaddress::create([
            'address' => 'test@example.com'
        ]);
        Mailaddress::create([
            'address' => 'test2@example.com',
            'company_id' => 1
        ]);
    }

    /** @test */
    public function it_will_list_mailaddresses()
    {
        $response = $this->get('api/mailaddress');
        $response->assertSuccessful();
        $response->assertJson([
            [ 'address' => 'test@example.com' ],
            [ 'address' => 'test2@example.com']
        ]);
    }

    /** @test */
    public function it_will_delete_a_mailaddress()
    {
        $response = $this->delete('api/mailaddress/1');
        $response->assertJson([
            'message' => 'Success'
        ]);
        $this->assertNull(Mailaddress::find(1));
    }
}
