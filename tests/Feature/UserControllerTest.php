<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $user = new User([
            'email' => 'test@example.com',
            'name' => 'testuser',
            'password' => '123456',
        ]);
        $user->save();
        $this->token = auth()->login($user);
    }

    /** @test */
    public function it_will_return_the_user_info()
    {
        $response = $this->get('api/user');
        $response->assertJsonStructure([
            'id',
            'name',
            'email',
            'created_at',
            'updated_at'
        ]);
    }

}
