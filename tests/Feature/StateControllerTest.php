<?php

namespace Tests\Feature;

use App\State;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StateControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $user = new User([
            'email' => 'test@example.com',
            'name' => 'testuser',
            'password' => '123456',
        ]);
        $user->save();
        $this->token = auth()->login($user);

        State::create([
            'title' => 'Open',
            'open' => true
        ]);
        State::create([
            'title' => 'Closed',
            'open' => false
        ]);
    }

    /** @test */
    public function it_will_add_state()
    {
        $response = $this->post('api/state', [
            'title' => 'Rejected',
            'open' => false
        ]);
        $response->assertCreated();
        $response->assertJson([
            'title' => 'Rejected'
        ]);
        $response = $this->post('api/state', [
            'title' => 'In Progress'
        ]);
        $response->assertJson([
            'title' => 'In Progress',
            'open' => true
        ]);
    }

    /** @test */
    public function it_will_list_states()
    {
        $response = $this->get('api/state');
        $response->assertJson([
            [ 'title' => 'Open' ],
            [ 'title' => 'Closed' ]
        ]);
    }

    /** @test */
    public function it_will_get_one_state()
    {
        $response = $this->get('api/state/2');
        $response->assertJson([
            'title' => 'Closed'
        ]);
    }

    /** @test */
    public function it_will_not_delete_state()
    {
        $response = $this->delete('api/state/1');
        $response->assertStatus(405);
        $this->assertNotNull(State::find(1));
    }

}
