<?php

namespace Tests\Feature;

use App\Category;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $user = new User([
            'email' => 'test@example.com',
            'name' => 'testuser',
            'password' => '123456',
        ]);
        $user->save();
        $this->token = auth()->login($user);

        Category::create([
            'title' => 'Support',
            'email' => 'support@example.com'
        ]);
        Category::create([
            'title' => 'Sales',
            'email' => 'sales@example.com'
        ]);
    }

    /** @test */
    public function it_will_create_category()
    {
        $response = $this->post('api/category', [
            'title' => 'Test Category'
        ]);
        $response->assertCreated();
        $response->assertJson([
            'title' => 'Test Category',
            'email' => null
        ]);

        $response = $this->post('api/category', [
            'title' => 'Test Category 2',
            'email' => 'test@example.com'
        ]);
        $response->assertJson([
            'email' => 'test@example.com'
        ]);
    }

    /** @test */
    public function it_will_list_categories()
    {
        $response = $this->get('api/category');
        $response->assertJson([
            [ 'title' => 'Support' ],
            [ 'title' => 'Sales' ]
        ]);
    }

    /** @test */
    public function it_will_edit_category()
    {
        $response = $this->put('api/category/1', [
            'email' => 'newsupport@example.com'
        ]);
        $response->assertJson([
            'email' => 'newsupport@example.com'
        ]);
        $category = Category::find(1);
        $this->assertEquals('newsupport@example.com', $category->email);
    }

    /** @test */
    public function it_will_delete_category()
    {
        $response = $this->delete('api/category/1');
        $response->assertJson([
            'message' => 'Success'
        ]);
        $this->assertNull(Category::find(1));
    }
}
