<?php

namespace Tests\Feature;

use App\Category;
use App\Contact;
use App\Mailaddress;
use App\State;
use App\Ticket;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TicketsControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $user = new User([
            'email' => 'test@example.com',
            'name' => 'testuser',
            'password' => '123456',
        ]);
        $user->save();
        $this->token = auth()->login($user);

        Category::create([
            'title' => 'Testcat'
        ]);
        Category::create([
            'title' => 'Testcat2'
        ]);
        State::create([
            'title' => 'Test'
        ]);
        Contact::create([
            'name' => 'Testcontact',
            'email' => 'test@contact.example',
            'phone' => '012 345 67 89',
            'company_id' => '1'
        ]);
        Mailaddress::create([
            'address' => 'test@example.com'
        ]);

        Ticket::create([
            'identifier' => 'identifier1',
            'contact_id' => 1,
            'from_id' => 1,
            'to_id' => 2,
            'title' => 'test ticket 1',
            'category_id' => 1,
            'state_id' => 1,
            'priority' => 1
        ]);
        Ticket::create([
            'identifier' => 'identifier2',
            'contact_id' => 2,
            'from_id' => 1,
            'to_id' => 2,
            'title' => 'test ticket 2',
            'category_id' => 1,
            'state_id' => 1,
            'priority' => 3
        ]);
        Ticket::create([
            'identifier' => 'identifier3',
            'contact_id' => 1,
            'from_id' => 1,
            'to_id' => 2,
            'title' => 'test ticket 3',
            'category_id' => 1,
            'state_id' => 1,
            'priority' => 3
        ]);
    }

    /** @test */
    public function it_will_list_open_tickets()
    {
        $response = $this->get('api/ticket?state=1');
        $response->assertJsonStructure([[
            'id',
            'identifier',
            'contact_id'
        ]]);
    }

    /** @test */
    public function it_will_list_all_tickets()
    {
        $response = $this->get('api/ticket');
        $response->assertJsonStructure([[
            'id',
            'identifier',
            'contact_id'
        ]]);
    }

    /** @test */
    public function it_will_list_all_tickets_of_category()
    {
        $response = $this->get('api/ticket?category=1');
        $response->assertJsonStructure([[
            'id',
            'identifier',
            'contact_id'
        ]]);
    }

    /** @test */
    public function it_will_give_error_for_unknown_categories()
    {
        $response = $this->get('api/ticket?category=50');
        $response->assertNotFound();
    }

    /** @test */
    public function it_will_return_one_ticket()
    {
        $response = $this->get('api/ticket/1');
        $response->assertJsonFragment([
            'identifier' => 'identifier1'
        ]);
    }

    /** @test */
    public function it_will_return_404_for_notexisting_ticket()
    {
        $response = $this->get('api/ticket/5000');
        $response->assertNotFound();
    }

    /** @test */
    public function it_will_create_new_ticket()
    {
        $response = $this->post('api/ticket', [
            'contact_id' => 1,
            'from_id' => 1,
            'to_id' => 1,
            'title' => 'Example subject',
            'category_id' => 1,
            'state_id' => 1,
            'priority' => 3
        ]);
        $response->assertJsonFragment([
            'title' => 'Example subject'
        ]);
    }
}
