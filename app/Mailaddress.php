<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mailaddress extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'address'
    ];

    public function tickets_to()
    {
        return $this->hasMany('App\Ticket', 'to_id');
    }

    public function tickets_from()
    {
        return $this->hasMany('App\Ticket', 'from_id');
    }

    public function messages_to()
    {
        return $this->hasMany('App\Message', 'to_id');
    }

    public function messages_from()
    {
        return $this->hasMany('App\Message', 'from_id');
    }
}
