<?php

namespace App\Http\Controllers;

use App\Category;
use App\Contact;
use App\Mailaddress;
use App\State;
use App\Ticket;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];


        if($request->has('category')) {
            $where[] = ['category_id', '=', $request->get('category')];
        }
        if($request->has('state')) {
            $where[] = ['state_id', '=', $request->get('state')];
        }
        if($request->has('contact')) {
            $where[] = ['contact_id', '=', $request->get('contact')];
        }
        if($request->has('priority')) {
            $where[] = ['priority', '=', $request->get('priority')];
        }

        return Ticket::where($where)->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
        throw_if(is_null(Category::find($data['category_id'])), NotFoundHttpException::class, 'Category not found');
        throw_if(is_null(Contact::find($data['contact_id'])), NotFoundHttpException::class, 'Contact not found');
        throw_if(is_null(Mailaddress::find($data['from_id'])), NotFoundHttpException::class, 'From not found');
        throw_if(is_null(Mailaddress::find($data['to_id'])), NotFoundHttpException::class, 'To not found');
        throw_if(is_null(State::find($data['state_id'])), NotFoundHttpException::class, 'State not found');

        $data['identifier'] = Uuid::uuid1();
        $ticket = Ticket::create($data);
        return $ticket;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        return $ticket;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
