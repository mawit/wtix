<?php

namespace App\Http\Controllers;

use App\Mailaddress;
use Illuminate\Http\Request;

class MailaddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Mailaddress::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mailaddress  $mailaddress
     * @return \Illuminate\Http\Response
     */
    public function show(Mailaddress $mailaddress)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mailaddress  $mailaddress
     * @return \Illuminate\Http\Response
     */
    public function edit(Mailaddress $mailaddress)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mailaddress  $mailaddress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mailaddress $mailaddress)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mailaddress  $mailaddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mailaddress $mailaddress)
    {
        $mailaddress->delete();
        return response([ 'message' => 'Success' ]);
    }
}
