<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'subject',
        'from_id',
        'to_id',
        'raw_message',
        'body'
    ];

    public function to()
    {
        return $this->belongsTo('App\Mailaddress', 'to_id');
    }

    public function from()
    {
        return $this->belongsTo('App\Mailaddress', 'from_id');
    }

    public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }
}
