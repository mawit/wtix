<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'open'
    ];

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }
}
