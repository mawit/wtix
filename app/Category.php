<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'title',
        'email'
    ];

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }
}
