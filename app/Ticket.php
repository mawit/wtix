<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'identifier',
        'contact_id',
        'from_id',
        'to_id',
        'title',
        'category_id',
        'state_id',
        'priority'
    ];

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

    public function from()
    {
        return $this->belongsTo('App\Mailaddress', 'from_id');
    }

    public function to()
    {
        return $this->belongsTo('App\Mailaddress', 'to_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }

    public function messages()
    {
        return $this->hasMany('App\Message');
    }
}
