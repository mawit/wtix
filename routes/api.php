<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');
Route::post('refresh', 'AuthController@refresh');

Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource('/user', 'UserController');
    Route::apiResource('contact', 'ContactController');
    Route::apiResource('mailaddress', 'MailaddressController')->except(([ 'store', 'update' ]));
    Route::apiResource('state', 'StateController')->except( [ 'destroy', 'update' ]);
    Route::apiResource('category', 'CategoryController');
    Route::apiResource('ticket', 'TicketController');
});

Route::name('notauthenticated')->get('notauthenticated', function () {
    return response([ 'error' => 'not authenticated'], 401);
});
