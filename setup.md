# Set up recommondations

## Set up in new environment

```bash
$ composer install [--no-dev]
$ php artisan migrate
$ php artisan key:generate
$ php artisan jwt:secret
```
