<?php

use App\State;
use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        State::create([
            'title' => 'New',
            'open' => true
        ]);

        State::create([
            'title' => 'In Progress',
            'open' => true
        ]);

        State::create([
            'title' => 'Closed',
            'open' => false
        ]);
    }
}
