<?php

use App\Mailaddress;
use Illuminate\Database\Seeder;

class MailaddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mailaddress::create([
            'address' => 'testaddress@example.com'
        ]);
        Mailaddress::create([
            'address' => 'testaddress2@example.com',
            'contact_id' => '1'
        ]);
    }
}
