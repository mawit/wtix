<?php

use App\Contact;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::create([
            'name' => 'Test Contact',
            'email' => 'test-contact@example.com',
            'phone' => '+41 12 345 67 89'
        ]);
    }
}
