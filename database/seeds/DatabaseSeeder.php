<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            ContactSeeder::class,
            CategorySeeder::class,
            MailaddressSeeder::class,
            MessageSeeder::class,
            StateSeeder::class,
            TicketSeeder::class,
        ]);
    }
}
