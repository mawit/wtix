<?php

use App\Message;
use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Message::create([
            'subject' => 'test subject',
            'from_id' => 1,
            'to_id' => 2,
            'body' => 'This is an example body',
            'raw_message' => <<<EOS
From: test@example.com
To: test2@example.com
Subject: test subject

This is an example body

EOS
        ]);
    }
}
