<?php

use App\Ticket;
use Illuminate\Database\Seeder;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ticket::create([
            'identifier' => 'example_identifier',
            'contact_id' => 1,
            'from_id' => 2,
            'to_id' => 1,
            'title' => 'Example ticket',
            'category_id' => 1,
            'state_id' => 2,
            'priority' => 2
        ]);
    }
}
